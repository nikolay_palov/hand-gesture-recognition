from errno import EXDEV
import json
from pathlib import Path
import subprocess
import os
import re
import traceback
import numpy as np

from action_recognition.validation import validate


class MLGesture():
    """A custom API for running MLGesture programmatically. Based on running MLGesture's main.py with 
    the appropriate shell commands. Can take different config arguments in order to customize runs.
    """
    __BASE_STRING__ = """
    [
        {\"video\": \"{0}\", \"label\": [\"swipe_right\"]},
        {\"video\": \"{0}\", \"label\": [\"swipe_left\"]},
        {\"video\": \"{0}\", \"label\": [\"swipe_left\"]},
        {\"video\": \"{0}\", \"label\": [\"swipe_up\"]},
        {\"video\": \"{0}\", \"label\": [\"swipe_down\"]},
        {\"video\": \"{0}\", \"label\": [\"turn_left\"]},
        {\"video\": \"{0}\", \"label\": [\"turn_right\"]},
        {\"video\": \"{0}\", \"label\": [\"push\"]},
        {\"video\": \"{0}\", \"label\": [\"wave\"]},
        {\"video\": \"{0}\", \"label\": [\"roll\"]},
        {\"video\": \"{0}\", \"label\": [\"no_gesture\"]}
    ]
    """
    __LABEL_NAMES__ = ['no_gesture', 'push', 'roll', 'swipe_down', 'swipe_left',
                       'swipe_right', 'swipe_up', 'turn_left', 'turn_right', 'wave']

    def __init__(self, _video_width=32, _video_height=32,
                 _cuda=True,
                 _backbone="resnet18_ms_tcn",
                 _model_path=Path("data/checkpoints/tcn_mix2_f128_ce.pth"),
                 _thread_num=4,
                 _runtime="torch"
                 ) -> None:
        if _video_height > 32 and _video_width > 32:
            raise ValueError(
                "Improper video dimensions. Both dimensions should be <=32")
        self.video_width = _video_width
        self.video_height = _video_height

        if _cuda:
            self.cuda = "--cuda"
        else:
            self.cuda = "--no-cuda"

        self.backbone = _backbone
        self.model_path = _model_path
        if _thread_num is None:
            self.n_threads = 4
        else:
            self.n_threads = _thread_num
        self.runtime = _runtime

        self.base_dir = Path(os.path.dirname(os.path.abspath(__file__)))

    def __generate_json__(self, video_path: Path) -> str:
        """
        Adds a custom path to the BASE_STRING template and generates the json string needed to run mlgesture with a
        single video
        """
        video_json = self.__BASE_STRING__.replace("{0}", video_path.as_posix())
        return video_json

    def __generate_database__(self, video_path: Path, output_path: Path):
        r = self.__generate_json__(video_path)
        common_json = json.loads(r)
        with open(output_path / Path("validation_custom.json"), "w") as v:
            json.dump(common_json, v)

        with open(output_path / Path("train_custom.json"), "w") as t:
            json.dump(common_json, t)

    def __run_mlgesture_datatool__(self, datatool_path: Path, video_dir: Path, json_dir: Path, output_dir: Path):
        print("===========Running datatool============")
        cmd_array = ["python3",
                     datatool_path.as_posix(),
                     video_dir.as_posix(),
                     (json_dir / Path("train_custom.json")).as_posix(),
                     (json_dir / Path("validation_custom.json")).as_posix(),
                     (output_dir / Path("mlgesture_mlx90640_front_custom.json")).as_posix(),
                     "mlx90640_front"
                     ]
        array_str = ' '.join(cmd_array)
        print(f"Datatool arguments {array_str}")
        run_result = subprocess.run(cmd_array, capture_output=True, text=True)
        if run_result.returncode != 0:
            print("Failed to run datatool")
            print(run_result.stdout)
            print(run_result.stderr)
            raise RuntimeError("Datatool run failed. Does video path exist?")

    def __run_mlgesture__(self, root_path: Path, video_path: Path):
        print("===========Running MLGesture============")
        cmd_array = ["python3", (self.base_dir / Path("main.py")).as_posix(),
                     "--root-path", (self.base_dir / root_path).as_posix(),
                     "--result-path", (self.base_dir
                                       / Path("data/logs/mlgesture_debug")).as_posix(),
                     "--dataset", "mlgesture_mlx90640_front",
                     "--model", self.backbone,
                     "--mean-dataset", "mlgesture_mlx",
                     "--max-pixel-value", "3000",
                     "--min-pixel-value", "-400",
                     "--num-stages", "4",
                     "--num-layers", "5",
                     "--num-f-maps", "128",
                     "--no-train",
                     "--no-val",
                     "--pretrain-path", (self.base_dir
                                         / self.model_path).as_posix(),
                     "--video-path", video_path.as_posix(),
                     "--sample-size", str(self.video_height), str(self.video_width),
                     "--n-threads", str(self.n_threads),
                     "--annotation-path", (self.base_dir / Path("data/MlGesture/mlgesture_mlx90640_front_custom.json")).as_posix(),
                     self.cuda
                     ]
        array_str = ' '.join(cmd_array)
        print(f"Running with command:{array_str}")

        cmd_result = subprocess.run(cmd_array, capture_output=True, text=True)
        print("===STDOUT===")
        print(cmd_result.stdout)
        print("===STDERR===")
        print(cmd_result.stderr)

        return cmd_result

    def __parse__clip_accuracy__(self, stdout):
        accuracies = np.array([float(acc.group(0).strip())
                              for acc in re.finditer("(?<=clip )(\d*\.?\d+)", stdout)])
        return accuracies

    def validate(self, root_path: Path, video_path: Path):
        """
        Runs the mlgesture validation procedure and returns the vector (np.ndarray) with all the accuracies,
        their mean and their standart deviation. Other statistics can be easily calculated via numpy.
        """
        print("===========Running MLGesture in Validation MODE============")
        cmd_array = ["python3", (self.base_dir / Path("main.py")).as_posix(),
                     "--root-path", (self.base_dir / root_path).as_posix(),
                     "--result-path", (self.base_dir
                                       / Path("data/logs/mlgesture_debug")).as_posix(),
                     "--dataset", "mlgesture_mlx90640_top",
                     "--model", self.backbone,
                     "--mean-dataset", "mlgesture_mlx",
                     "--max-pixel-value", "3000",
                     "--min-pixel-value", "-400",
                     "--num-stages", "4",
                     "--num-layers", "5",
                     "--num-f-maps", "128",
                     "--no-train",
                     "--val",
                     "--no-test",
                     "--pretrain-path", (self.base_dir
                                         / self.model_path).as_posix(),
                     "--video-path", video_path.as_posix(),
                     "--sample-size", str(self.video_height), str(self.video_width),
                     "--n-threads", str(self.n_threads),
                     self.cuda
                     ]
        array_str = ' '.join(cmd_array)
        print(f"Running with command:{array_str}")

        cmd_result = subprocess.run(cmd_array, capture_output=True)

        # manual decoding was required due to failing automatic utf8 decode. Decoding to ascii with
        # the option to ignore unknown bytes
        stdout = cmd_result.stdout.decode("ascii", "ignore")
        stderr = cmd_result.stderr.decode("ascii", "ignore")

        print("===STDOUT===")
        print(stdout)
        print("===STDERR===")
        print(stderr)
        print("Parsing accuracies found during validation procedure")
        accuracies_vector = self.__parse__clip_accuracy__(stdout)
        print(f'Clip accuracies vector {accuracies_vector}')

        return accuracies_vector, np.mean(accuracies_vector), np.std(accuracies_vector)
    
    def export_to_onnx(self, onnx_output_path: Path):
        print((self.base_dir / Path("./data")).as_posix())
        cli_res = subprocess.run(["python3", (self.base_dir / Path("main.py")).as_posix(),
                                  "--root-path", (self.base_dir
                                                  / Path("./data")).as_posix(),
                                 "--result-path", (self.base_dir
                                                   / Path("logs/mlgesture_debug")).as_posix(),
                                  "--dataset", "mlgesture_mlx90640_front",
                                  "--model", "resnet18_ms_tcn",
                                  "--onnx", onnx_output_path,
                                  "--input-size", "3"
                                  ])
        print(cli_res.stdout)
        print(cli_res.stderr)

    def __parse_stdout_to_label_number__(self, stdout_result: str) -> str:
        """
        Regex-es mlgesture's CLI output to find the inference result
        Expects to find a line in the form clip_pred=tensor([d, d, d, d, d, d, d, d]) where d
        is a digit correspoding to the label in the stdout (str) of mlgesture
        """
        label_match = [line.strip() for line in stdout_result.split(
            "\n") if "clip_pred=" in line]
        gesture_label = re.search("\[.*?\]", label_match[0])
        label = json.loads(gesture_label.group(0))[0]

        return label

    def classify_video(self, video_path: Path):
        """
        The main user-facing function that takes the path to a video and returns the inference result
        as a tuple of the format (label_id: int, label_name: str)
        """
        out_path = Path("data/MlGesture")

        mlgesture_datatool = self.base_dir / Path("utils/mlgesture_json.py")
        self.__generate_database__(video_path=video_path,
                                   output_path=self.base_dir / out_path)

        self.__run_mlgesture_datatool__(datatool_path=mlgesture_datatool,
                                        video_dir=video_path,
                                        json_dir=(self.base_dir / out_path),
                                        output_dir=(self.base_dir / out_path)
                                        )
        run_result = self.__run_mlgesture__(root_path=Path("./data"),
                                            video_path=video_path
                                            )
        try:
            label_id = self.__parse_stdout_to_label_number__(run_result.stdout)
            return label_id, self.__LABEL_NAMES__[label_id]
        except Exception as ex:
            print("Failed to parse source video")
            traceback.print_exc(ex)
            return None, None


def main():
    """
    A simple test IF that takes a video path as a CLI argument (a directory containing an appropriately 
    named sequence of tiffs) and returns the inference result
    """
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('video_path')
    args = parser.parse_args()

    video_path = Path(args.video_path)
    ml = MLGesture()
    # run with python3 ./source/hand-gesture-recognition/MLGestureAPI.py ./MlGesture/frame_data
    # ml.validate(Path("./data"), video_path)
    print(ml.classify_video(video_path)) # run with python3 ./source/hand-gesture-recognition/MLGestureAPI.py /home/emil/Projects/driver-gesture-recognition_v2/source/hand-gesture-recognition/data/MlGesture/frame_data/label03/subject1/video0


if __name__ == "__main__":
    main()
